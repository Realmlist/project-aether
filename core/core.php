<?php
	// Error reporting in dev mode
	error_reporting(E_ALL);

	// Loading in IPBWI API
		 include_once('lib/ipbwi/ipbwi.inc.php');
	// Setting IPBWI Member vars
		 $member = $ipbwi->member->info();
	// Database connection
		 $link = mysql_connect('localhost', '', '');
			if (!$link) {
				die('Could not connect to the database: ' . mysql_error());
			}
	// Gameserver Auth & Character SQL
		 // Get gameserver account details
		 $auth_q = mysql_query("SELECT * FROM auth.account WHERE username = '" . strtoupper($member['name']) . "'");
		 $auth = mysql_fetch_array($auth_q, MYSQL_ASSOC);
		 // See if the user has a GM account
		 $GMauth_q = mysql_query("SELECT * FROM auth.account WHERE username = 'STAFF" . strtoupper($member['name']) . "'");
		 $GMauth = mysql_fetch_array($GMauth_q, MYSQL_ASSOC);
         // If it does, use the GM account ID instead of the player account ID:
         $id = mysql_num_rows($GMauth_q) == 1 ? $GMauth['id'] : $auth['id'];
		 // Get gameserver character & account access details for used account ID.
		 $char_sql = "SELECT * FROM characters.characters, auth.account_access 
		 						WHERE characters.characters.account = auth.account_access.id
								AND characters.characters.account = '" . $id . "'";
		 $char_q = mysql_query($char_sql);
		 $char = mysql_fetch_array($char_q, MYSQL_ASSOC);
		 if (!isset($char['gmlevel'])){ // If no GM LVL is set
			 $char['gmlevel'] = "0";
		 }
	// GM CP Access filter
		if($_GET['p'] == "gm"){
			if(!$ipbwi->member->isLoggedIn()){
				$error = "You're not logged in!";
			}else{
				$fGroups = array (4,10,11); // 4 = Admin, 10 = GM, 11 = Trial GM
				if(!$ipbwi->group->isInGroup($fGroups)){
					$error = "You have no permission to be here!";
				}
				if ($char['gmlevel'] == 0){ // If no GM LVL is set
				  $error = "You don't have a GM account linked to this account.<br />";
				  $error .= "Contact an administrator.";
				}
			}
		}
	// Load SOAP module/lib once in GM CP
	if($_GET['p'] == "gm"){
		require_once('lib/SOAP.php');
	}
?>