<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$("#sub").change(function () {
    if ($(this).val() == 'player') {
        $("#sub2").prop("disabled", false);
    } else {
        $("#sub2").val('');
        $("#sub2").prop("disabled", true);

    }
});
});//]]>  

</script>
<form action="" method="post">
	<legend>Lookup:</legend>
    <select name="sub" id="sub">
      <option value="item">Item</option>
      <option value="creature">Creature</option>
      <option value="object">Gameobject</option>
      <option value="player">Player</option>
    </select>
    <select name="sub2" id="sub2" disabled>
      <option value="">Subcommand</option>
      <option value="acc">Account</option>
      <option value="ip">IP</option>
      <option value="email">Email</option>
    </select>
    <input type="text" name="string" />
    <input type="submit" value="Submit" name="Lookup" />
</form>
<?php

if(isset($_POST['Lookup']) && empty($_POST['sub2'])){
	if($_POST['sub'] == "player"){
		$command = '.lo ' . $_POST['sub'] . ' ' . $_POST['string'];
		echo "DEBUG: Command: \"".$command . "\"<br /><br />";
		echo "Select a sub-command";
		break;
	}
		$command = '.lo ' . $_POST['sub'] . ' ' . $_POST['string'];
		$command_output = ReturnAetherCommand($command, 'STAFFREALMLIST', ACCPW);
			echo "DEBUG: Command: \"".$command . "\"<br /><br />";
			
			$lines = explode("\n", trim($command_output));
			echo "<table width='500px' border=1><tr><td><strong>ID</strong></td><td><strong>Name</strong></td></tr>";
			foreach($lines as $line) {
			  echo "<tr>";
			
			  $elements = explode("-", $line, 2);
			  foreach($elements as $element) {
				echo "<td>" . $element . "</td>";
			  }
			  echo "</tr>";
			}
			echo "</table>";
}elseif(isset($_POST['Lookup']) && !empty($_POST['sub2'])){
	$command = '.lo ' . $_POST['sub'] . ' ' . $_POST['sub2'] . ' ' . $_POST['string'];
	$command_output = ReturnAetherCommand($command, 'STAFFREALMLIST', ACCPW);
	echo "DEBUG: Command: \"".$command . "\"<br /><br />";
	echo nl2br($command_output);
}
?>