<div align="center">
<?php

	if(isset($_POST['action']) && $_POST['action'] == 'register'){
		
		$username = mysql_real_escape_string($_POST['username']);
		$email = mysql_real_escape_string($_POST['email']);
		$displayname = mysql_real_escape_string($_POST['displayname']);
		$password = mysql_real_escape_string($_POST['password']);
		$password_control = mysql_real_escape_string($_POST['password_control']);
		
		if($password != $password_control){
			$ipbwi->addSystemMessage('Error','You have to type the same password for control.');
		}else{
			// Q&A Control if needed
			$question = $GLOBALS['ipbwi']->antispam->qandaGet();
			if($question !== false){
				if(!$GLOBALS['ipbwi']->antispam->qandaCheck()){
					$GLOBALS['ipbwi']->addSystemMessage('Error', 'Answer of Q&amp;A Challenge not correct');
				}
			}
			// convert custom_fields in a custom array
			// first check, which custom fields exists
			$customFields = $ipbwi->member->listCustomFields();
			// load fielddatas
			foreach($customFields as $field){
				// get delivered field datas
				if(isset($_POST['field_'.$field['pf_id']])){
					$fieldDatas['field_'.$field['pf_id']] = $_POST['field_'.$field['pf_id']];
				}else{
					$fieldDatas['field_'.$field['pf_id']] = '';
				}
			}
			if($ipbwi->member->create($username, $_POST['password'], $email, $fieldDatas, true, $displayname, true, false, true)){
				// Make in-game account:
					$user =  mysql_real_escape_string($_POST['username']);
					$pass =  mysql_real_escape_string($_POST['password']);
					$email = mysql_real_escape_string($_POST['email']);
					$hash = sha1(strtoupper($user).':'.strtoupper($pass));
					$ip = $_SERVER['REMOTE_ADDR'];
					mysql_select_db('auth', $link) or die('Could not select database.');
					mysql_query("INSERT INTO account (username, sha_pass_hash, email, last_ip) VALUES ('".$user."','".$hash."','".$email."', '".$ip."')") or die(mysql_error());
				// Log in on the website & forums
				if($ipbwi->member->login($_POST['username'], $_POST['password'])){
					header('location: '.ipbwi_WEB_URL).die();
				}
			}
		}
	}



	// Error Output
	echo $ipbwi->printSystemMessages();

	if($ipbwi->member->isLoggedIn()){
?>
		<p>You are already logged in!</p>
<?php
	}else{
?>
		<form action="/create/" method="post">
			<table>
				<tr><td colspan="2"><p><strong>Fields with * must be filled out.</strong></p></td></tr>
				<tr><td>Username*</td><td><input style="width: 200px;" name="username" type="text" /></td></tr>
				<tr><td>Email*</td><td><input style="width: 200px;" name="email" type="text" /></td></tr>
				<tr><td>Password*</td><td><input style="width: 200px;" name="password" type="password" /></td></tr>
				<tr><td>Password*</td><td><input style="width: 200px;" name="password_control" type="password" /></td></tr>
				<tr><td>Display Name</td><td><input style="width: 200px;" name="displayname" type="text" /></td></tr>
                <tr><td colspan="2">
				<?php echo $ipbwi->antispam->getHTML('anti_spam.php?renewImage=true'); ?>
                </td></tr>
				<tr><td colspan="2"><input name="register" value="Register" type="submit" /><input name="action" value="register" type="hidden" /></td></tr>
			</table>
		</form>
        </div>
<?php
	}

?>