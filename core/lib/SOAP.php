<?php
	function ExecuteAetherCommand($cmd, $uname, $pass){
		try{
			$client = new SoapClient(NULL,
				array(
					'location' 	=> 'http://127.0.0.1:7878', //SOAP.IP && SOAP.Port
					'uri'		=> 'urn:TC',
					'style'		=> SOAP_RPC,
					'login'		=> $uname,
					'password'	=> $pass,
				));
			$client->executeCommand(new SoapParam($cmd, 'command'));
		}
		catch(SoapFault $e){
			die('SOAP server is down.');
		}
	}

	function ReturnAetherCommand($cmd, $uname, $pass){
		try{
			$client = new SoapClient(NULL,
				array(
					'location'   => 'http://127.0.0.1:7878',
					'uri'		 => 'urn:TC',
					'style'		 => SOAP_RPC,
					'login'		 => $uname,
					'password'   => $pass
				));

			return ($client->executeCommand(new SoapParam($cmd, 'command')));
		}
		catch(SoapFault $e){
			echo '<p><strong>Either:<ul>	<li>The server is down.</li> 
											<li>You don\'t have the permissions to use this command.</li> 
											<li>You made an invalid command.</li> 
											<li>Nothing can be found/done.</li>
					</ul></strong></p>';
		}
	}

?>