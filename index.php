<?php
	//HTML & PHP by Realmlist
	define('FILE_PATH', 'http://' . $_SERVER['SERVER_NAME'] . '/');
	require_once('core/core.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Aether RP ~</title>
<link href="<? print FILE_PATH; ?>css/main.css" rel="stylesheet" type="text/css">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="<? print FILE_PATH; ?>core/lib/jquery.slides.min.js"></script>
<script>
	 $(function(){
      $("#scrollbar").slidesjs({
   		height:270,
		navigation:false,
		pagination:false,
		effect:{
			slide:{
				speed:700
				}
			},
		play: {
			active:false,
			auto:true
		}
      });
    });
</script>
</head>
<body>
	<div id="wrapper">
    	<header>
        	<a href="<?php echo FILE_PATH; ?>"><img src="<? echo FILE_PATH; ?>img/logo.png" alt="Aether RP logo" id="logo" /></a>
            <menu>
              <ul>
               <?php
				if($_GET['p'] == "gm" && !isset($error)){
					echo '<li><a href="'. FILE_PATH.'">Home</a></li>';
					echo '<li><a href="/gm/lookup/">Lookup Tool</a></li>';
					echo '<li><a href="/gm/pinfo/">Player Info</a></li>';
                    echo '<li><a href="/gm/logs/">GM logs</a></li>';
					
					echo '<li><a href="/logout/">Log Out</a></li>';
					
				}else{
					if($ipbwi->member->isLoggedIn()){
						echo '<li><a href="'. FILE_PATH.'">Home</a></li>';
               			echo '<li><a href="/forums/">Forums</a></li>';
						echo '<li><a href="/account/">My Account</a></li>';
						echo '<li><a href="/connection-guide/">Connection Guide</a></li>';
					}
					if(!$ipbwi->member->isLoggedIn()){
						echo '<li><a href="'. FILE_PATH.'">Home</a></li>';
               			echo '<li><a href="/forums/">Forums</a></li>';
						echo '<li><a href="'.FILE_PATH.'forums/index.php?app=core&amp;module=global&amp;section=login">Log In</a></li>';
						echo '<li><a href="/create/">Create account</a></li>';
					}else{
						echo '<li><a href="/logout/">Log Out</a></li>';
					}
				}
			   ?>
              </ul>
            </menu>
		</header>
        </div>
        <div id="scrollbar">
        	<img src="<? print FILE_PATH; ?>img/bar.png" alt="Aether Bar">
            <img src="<? print FILE_PATH; ?>img/bar2.png" alt="Placeholder Bar">
        </div>
        <div id="wrapper2">
              <?php
			  if($_GET['p'] == NULL || empty($_GET['p']) || !isset($_GET['p'])){ require_once('core/pages/home.php'); } // Default page to show
			  elseif($_GET['p'] == "account")		{ 		require_once('core/pages/account.php');		}  //Account page
			  elseif($_GET['p'] == "logout")		{ 		require_once('core/pages/logout.php');		}  //Logout page
			  elseif($_GET['p'] == "create")		{ 		require_once('core/pages/create.php');		}  //Create user page
			  elseif($_GET['p'] == "connection-guide")		{ 		require_once('core/pages/guide.php');		}  //Create user page
			  
			  elseif($_GET['p'] == "gm")			{ 	//GM CP	
			  	if(isset($error))	{	echo $error;	break;	} // Access Filter
				if($_GET['p'] == "gm" && $_GET['p2'] == "lookup")	{ 	require_once('core/pages/gm/lookup.php');} // GM CP Lookup
				elseif($_GET['p'] == "gm" && $_GET['p2'] == "pinfo")	{ 	require_once('core/pages/gm/pinfo.php');} // GM CP Pinfo
                elseif($_GET['p'] == "gm" && $_GET['p2'] == "logs")	{ 	require_once('core/pages/gm/log.php');} // GM CP Pinfo
				else{	require_once('core/pages/gm/index.php');} // Default page
				} 
			  else{ require_once('404.php'); }	  // 404 not found page
			  ?>
        </div>
</body>
</html>
